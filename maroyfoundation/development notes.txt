{ {
 Using wget, i downloaded the entire website with all files. These are in:
   C:\cygwin64\home\Jordan Rowles\wget-output\maroyfoundation
 In LINKED ITEMS, just take the filename of the uri and file it in dir
} }

To do:
	[ ] Link contact form to Gmail SMTP server
	[ ] Fix social links in the footer


┍==============================================================================┑
|       MENU ITEM	|                         URI				                |
|===============================================================================|
| Home			    | https://www.MaroyFoundation.com/			                |
|  About Us		    | https://www.MaroyFoundation.com/about-us.html		        |
|  Out Services		| https://www.MaroyFoundation.com/our-services.html	        |
|  Projects		    | https://www.MaroyFoundation.com/projects.html		        |
|  Contact Us		| https://www.MaroyFoundation.com/contact-us.html	        |
|  Frequently Asked	| https://www.MaroyFoundation.com/frequently-asked.html	    |
┕==============================================================================┙

  LINKED ITEMS
=================================================================================
Programme start dates       	10536....pdf
Manifesto						Manifesto_2015.pdf
Privacy							Privacy_Policy.pdf
Terms & co						Terms_of_Conditions.pdf


  OBSERVATION NOTES
=================================================================================
	- Current website anchors to a few pdf documents
	- There's a form for contact
	- MaroyFoundation.com currently uses:
		+ Apache 2.2.29
		+ running on a UNIX system
		+ uses just jQuery
	- m.MaroyFoundation.com currently uses:
		+ Nginx 1.4.4
		+ Twitter Bootstrap
		+ jQuery 1.11.1
		+ jQuery UI
	! Are these websites on different hosts? (wtf?)
	^ Why not just have one site, with a responsive view for mobile/tablet?
	! On mobile 'Projects' is called 'International Projects'
	! Mobile has links to 'Full Website', 'Facebook'
	! On mobile: is that an aggregator on the front page? or static?
	^ Aggregator, on 'Facebook' has Maroy Foundation updates

  DEVELOPMENT NOTES
=================================================================================
	- Ideal production environment:
		+ Running a linux flavour (preferably debian)
		+ Apache HTTP server
		+ HTML5, CSS3, jQuery
		  ^ Own CSS from spliced project (like pizza site)
----------------------------------------------------------------------------------

