/**
 * Created by Jordan Rowles on 09/09/2015.
 */

$(document).ready(function () {
    /*
    $(".debuggingInfo").append("<p>Writing browser information to console...</p>");
    window.console&&console.log("Browser name: " + $.browser.name);
    window.console&&console.log("Browser ver: " + $.browser.version);
    window.console&&console.log("Layout engine: " + $.layout.name);
    window.console&&console.log("Layout ver: " + $.layout.version);
    window.console&&console.log("OS name: " + $.os.name);
    $(".debuggingInfo").append("<p>Done!</p>");
    */

    $(".debugBrowserName").append($.browser.name);
    $(".debugBrowserVersion").append($.browser.version);
    $(".debugLayoutName").append($.layout.name);
    $(".debugLayoutVersion").append($.layout.version);
    $(".debugOSName").append($.os.name);

    if (!window.jQuery)
    {
        $(".is_jQuery").append("jQuery is not running, at least not in this webpage anyway!");
    }
    else
    {
        $(".is_jQuery").append("jQuery loaded and ready!");
    }
});